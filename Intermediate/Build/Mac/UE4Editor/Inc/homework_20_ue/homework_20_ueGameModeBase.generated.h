// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK_20_UE_homework_20_ueGameModeBase_generated_h
#error "homework_20_ueGameModeBase.generated.h already included, missing '#pragma once' in homework_20_ueGameModeBase.h"
#endif
#define HOMEWORK_20_UE_homework_20_ueGameModeBase_generated_h

#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_SPARSE_DATA
#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_RPC_WRAPPERS
#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAhomework_20_ueGameModeBase(); \
	friend struct Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics; \
public: \
	DECLARE_CLASS(Ahomework_20_ueGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20_ue"), NO_API) \
	DECLARE_SERIALIZER(Ahomework_20_ueGameModeBase)


#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAhomework_20_ueGameModeBase(); \
	friend struct Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics; \
public: \
	DECLARE_CLASS(Ahomework_20_ueGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20_ue"), NO_API) \
	DECLARE_SERIALIZER(Ahomework_20_ueGameModeBase)


#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Ahomework_20_ueGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Ahomework_20_ueGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Ahomework_20_ueGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Ahomework_20_ueGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Ahomework_20_ueGameModeBase(Ahomework_20_ueGameModeBase&&); \
	NO_API Ahomework_20_ueGameModeBase(const Ahomework_20_ueGameModeBase&); \
public:


#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Ahomework_20_ueGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Ahomework_20_ueGameModeBase(Ahomework_20_ueGameModeBase&&); \
	NO_API Ahomework_20_ueGameModeBase(const Ahomework_20_ueGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Ahomework_20_ueGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Ahomework_20_ueGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Ahomework_20_ueGameModeBase)


#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_12_PROLOG
#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_SPARSE_DATA \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_RPC_WRAPPERS \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_INCLASS \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_SPARSE_DATA \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK_20_UE_API UClass* StaticClass<class Ahomework_20_ueGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID homework_20_ue_Source_homework_20_ue_homework_20_ueGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
