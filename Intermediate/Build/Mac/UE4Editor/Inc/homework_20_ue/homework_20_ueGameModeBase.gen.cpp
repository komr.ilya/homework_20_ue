// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "homework_20_ue/homework_20_ueGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodehomework_20_ueGameModeBase() {}
// Cross Module References
	HOMEWORK_20_UE_API UClass* Z_Construct_UClass_Ahomework_20_ueGameModeBase_NoRegister();
	HOMEWORK_20_UE_API UClass* Z_Construct_UClass_Ahomework_20_ueGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_homework_20_ue();
// End Cross Module References
	void Ahomework_20_ueGameModeBase::StaticRegisterNativesAhomework_20_ueGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_Ahomework_20_ueGameModeBase_NoRegister()
	{
		return Ahomework_20_ueGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_homework_20_ue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "homework_20_ueGameModeBase.h" },
		{ "ModuleRelativePath", "homework_20_ueGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Ahomework_20_ueGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics::ClassParams = {
		&Ahomework_20_ueGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Ahomework_20_ueGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Ahomework_20_ueGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Ahomework_20_ueGameModeBase, 4009164911);
	template<> HOMEWORK_20_UE_API UClass* StaticClass<Ahomework_20_ueGameModeBase>()
	{
		return Ahomework_20_ueGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Ahomework_20_ueGameModeBase(Z_Construct_UClass_Ahomework_20_ueGameModeBase, &Ahomework_20_ueGameModeBase::StaticClass, TEXT("/Script/homework_20_ue"), TEXT("Ahomework_20_ueGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Ahomework_20_ueGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
