// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    ElementSize = 60.f;    // implementation the variable for size of a element
    MovementSpeed = 0.5;   // implementation the variable for speed of the Snake
    DefaultElementsNum = 4;
    LastMoveDirection = EMovementDirection::DOWN;   // implementation last direction with value DOWN
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();     // "Super" - is call to the method of parent
    SetActorTickInterval(MovementSpeed);    // sets tick interval speed of the Snake movement
    AddSnakeElement(DefaultElementsNum); // sets default number of Snake elements
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    Move(); // movement of the Snake every frame

}

void ASnakeBase::AddSnakeElement(int ElementsNum)   // adding sets quantity elements (default is 1)
{
    for (int i = 0; i < ElementsNum; ++i)
    {
        if(SnakeElements.Num() < DefaultElementsNum)
        {
            FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);   // variable with calculation quantity elements and multiplication on size with vector type
            FTransform NewTransform(NewLocation);   // variable with transform type using vector variable with location
            ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform); // spawn actor element of snake with class SnakeElementClass and new transform with type SnakeElementBase and all write to variable with pointer on type SnakeElementBase
            NewSnakeElem->SnakeOwner = this;    // variable (field) with value of this class
            int32 ElementIndex = SnakeElements.Add(NewSnakeElem);   // return an index of added element in array the SnakeElements
            if (ElementIndex == 0) {                    // if index equal 0
                NewSnakeElem->SetFirstElementType();    // then add field in the element with function sets first element
            }
        }
        else
        {
            auto LastElement = SnakeElements[SnakeElements.Num()-1];
            FVector LastLocation = LastElement->GetActorLocation();
            FTransform NewTransform(LastLocation);
            
            ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
            NewSnakeElem->SnakeOwner = this;
            SnakeElements.Add(NewSnakeElem);
        }
    }
}

void ASnakeBase::Move()
{
    FVector MovementVector(ForceInitToZero);    // hard initialization to zero?
    
    switch (LastMoveDirection)                  // moving the Snake every tick in the last direction
    {
        case EMovementDirection::UP:            // if last direction is UP, then Snake moving each tick in UP
            MovementVector.X += ElementSize;    // the vector of movement with value of the element size
            break;
            
        case EMovementDirection::DOWN:          // similarly
            MovementVector.X -= ElementSize;
            break;
        
        case EMovementDirection::LEFT:          // similarly
            MovementVector.Y += ElementSize;
            break;
            
        case EMovementDirection::RIGHT:         // similarly
            MovementVector.Y -= ElementSize;
            break;
    }
    
    //AddActorWorldOffset(MovementVector);
    
    SnakeElements[0]->ToggleCollision();    // collision is disabled for the Snake's head
    
    for (int i = SnakeElements.Num() - 1; i > 0; i--)   // this cycle moving each element of the Snake one by one element starting with last
    {
        auto CurrentElement = SnakeElements[i];         // variable for storing the current element
        auto PrevElement = SnakeElements[i-1];          // variablr for storing the previous element
        FVector PrevLocation = PrevElement->GetActorLocation();     // variable for storing location the previous elements
        CurrentElement->SetActorLocation(PrevLocation); // sets location previous element for current element
    }
    
    SnakeElements[0]->AddActorWorldOffset(MovementVector);  // after moving all elements moving the head of the Snake by one movement vector
    SnakeElements[0]->ToggleCollision();                    // collision is enabled for the Snake's head
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)   // function for tracking overlapping elements
{
    if(IsValid(OverlappedElement))      // checking validation for existences overlapped element
    {
        int32 ElemIndex;
        SnakeElements.Find(OverlappedElement, ElemIndex);   // finding index overlapped element and writing index into variable
        bool bIsFirst = ElemIndex == 0;     // checking the Snake's head, writing boolean value
        
        IInteractable* InteractableInterface = Cast<IInteractable>(Other);  // writing in variable value with type Interactable from other actor
        if(InteractableInterface)       // checking validation for existences interactable actor (check interface into actor)
        {
            InteractableInterface->Interact(this, bIsFirst);    // checking this actor (SnakeBase) for interaction depending on the boolean variable. If interaction happens with snake's head, then interaction happens, otherwise interaction not happens
        }
    }
}
