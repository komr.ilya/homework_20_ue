// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;    // forward declaration

UENUM()
enum class EMovementDirection   // enum to define directions of the Snake
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};

UCLASS()
class HOMEWORK_20_UE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
    
    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<ASnakeElementBase> SnakeElementClass;   // definition variable subclass type SnakeElementBase
    
    UPROPERTY(EditDefaultsOnly)
    float ElementSize;  // definition variable for the size of element with type float
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MovementSpeed;    // definition variable for the speed of the Snake with type float
    
    UPROPERTY(EditDefaultsOnly)
    int32 DefaultElementsNum;   // default number elements for start game
    
    UPROPERTY(BlueprintReadWrite)
    TArray<ASnakeElementBase*> SnakeElements;   // definition array with type SnakeElemmentBase
    
    UPROPERTY()
    EMovementDirection LastMoveDirection;   // definition variable for remember last direction of the Snake

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    
    UFUNCTION(BluePrintCallable)                // ability to call in BP
    void AddSnakeElement(int ElementsNum = 1);  // definition function for adding elements into the Snake with default value 1
    
    UFUNCTION(BluePrintCallable)    // ability to call in BP
    void Move();    // definition function for moves the Snake
    
    UFUNCTION()
    void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other); //definition function for overlapped elems

};
