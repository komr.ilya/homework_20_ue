// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Math/RandomStream.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
    
    // AddFoodElement();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)  // function for interaction with the Snake's head
{
    if(bIsHead) // checking the head exists
    {
        auto ThisFood = this;
        auto Snake = Cast<ASnakeBase>(Interactor);  // create variable Snake with class SnakeBase
        auto CurrentSnakeSpeed = Snake->MovementSpeed;
        auto NewSnakeSpeed = CurrentSnakeSpeed * 0.9;
        if(IsValid(Snake))  // checking the Snake exists
        {
            Snake->AddSnakeElement();   // adding one element
            AddFoodElement(Snake);
        }
        ThisFood->Destroy();
    }
} 

void AFood::AddFoodElement(ASnakeBase* Snake)
{
    float RandX = FMath::FRandRange(-500.f, 500.f);
    float RandY = FMath::FRandRange(-500.f, 500.f);
    FVector FoodLocation(RandX, RandY, 30);
    
    for (int i = 0; i < Snake->SnakeElements.Num(); i++)
    {
        auto Element = Snake->SnakeElements[i];
        FVector ElementLocation = Element->GetActorLocation();
        
        if (FoodLocation.Equals(ElementLocation, Snake->ElementSize))
        {
            i = 0;
            RandX = FMath::FRandRange(-500.f, 500.f);
            RandY = FMath::FRandRange(-500.f, 500.f);
            FoodLocation = FVector(RandX, RandY, 30);
        }
    }
    
    FTransform FoodTransform(FoodLocation);
    GetWorld()->SpawnActor<AFood>(FoodActorClass, FoodTransform);
    

//    int FoodElemsNum = FoodActors.Num();
//    GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("FoodElemsNum == %d"), FoodElemsNum ));
//    UE_LOG(LogTemp, Warning, TEXT("Current values are: vector %s, float %f, and integer %d"), *YourVector.ToString(), YourFloat, YourInteger);
}
