// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "SnakeElementBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));  // create the camera actor with title "PawnCamera"
    RootComponent = PawnCamera;     // sets the camera in the root component
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
    SetActorRotation(FRotator(-90, 0, 0));      // set rotation for the camera actor
    CreateSnakeActor();                         // creating the Snake
    CreateFoodActor(SnakeActor);                // creating the Food
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);      // binding vertical inputs
    PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);  // binding horizontal inputs
}

void APlayerPawnBase::CreateSnakeActor()    // function is create the Snake with class SnakeBase
{
    SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::CreateFoodActor(ASnakeBase* Snake)    // function is create a food
{
    float RandX = FMath::FRandRange(-500.f, 500.f);
    float RandY = FMath::FRandRange(-500.f, 500.f);
    FVector FoodLocation(RandX, RandY, 30);
    
    for (int i = 0; i < Snake->SnakeElements.Num(); i++)
    {
        auto Element = Snake->SnakeElements[i];
        FVector ElementLocation = Element->GetActorLocation();
        
        if (FoodLocation.Equals(ElementLocation, Snake->ElementSize))
        {
            i = 0;
            RandX = FMath::FRandRange(-500.f, 500.f);
            RandY = FMath::FRandRange(-500.f, 500.f);
            FoodLocation = FVector(RandX, RandY, 30);
        }
    }
    
    FTransform FoodTransform(FoodLocation);
    FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, FoodTransform);
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
    if (IsValid(SnakeActor))    // checking the Snake exists
    {
        if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN) // checking vertical input
        {
            SnakeActor->LastMoveDirection = EMovementDirection::UP;                 // re-definition the last direction
        }
        else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)  // checking vertical input
        {
            SnakeActor->LastMoveDirection = EMovementDirection::DOWN;                   // re-definition the last direction
        }
    }
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
    if (IsValid(SnakeActor))    // checking the Snake exists
    {
        if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT) // checking horizontal input
        {
            SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;              // re-definition the last direction
        }
        else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)   // checking horizontal input
        {
            SnakeActor->LastMoveDirection = EMovementDirection::LEFT;                       // re-definition the last direction
        }
    }
}
