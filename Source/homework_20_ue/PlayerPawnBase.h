// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"


class UCameraComponent;     // forward declaration, acts like #include
class ASnakeBase;
class AFood;
class ASnakeElementBase;

UCLASS()
class HOMEWORK_20_UE_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();
    
    UPROPERTY(BlueprintReadWrite)
    UCameraComponent* PawnCamera;   // declaration variable for scene camera
    
    UPROPERTY(BlueprintReadWrite)
    ASnakeBase* SnakeActor;         // declaration variable for the actor Snake
    
    UPROPERTY(BlueprintReadWrite)
    AFood* FoodActor;         // declaration variable for the actor Snake
    
    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<ASnakeBase> SnakeActorClass;    // declaration variable wich contains SnakeBase class
    
    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<AFood> FoodActorClass;    // declaration variable wich contains Food class
    
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
    
    void CreateSnakeActor();
    void CreateFoodActor(ASnakeBase* Snake);
    
    UFUNCTION()
    void HandlePlayerVerticalInput(float value);        // function for controlling the Snake by vertical input
    
    UFUNCTION()
    void HandlePlayerHorizontalInput(float value);      // function for controlling the Snake by horizontal input

};
