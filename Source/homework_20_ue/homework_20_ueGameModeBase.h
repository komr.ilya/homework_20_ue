// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "homework_20_ueGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_20_UE_API Ahomework_20_ueGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
