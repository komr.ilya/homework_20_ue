// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;     // forward declaration, like the directive #include
class ASnakeBase;               // forward declaration, like the directive #include

UCLASS()
class HOMEWORK_20_UE_API ASnakeElementBase : public AActor, public IInteractable    // public inheritance from Actor class and Interactable class
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)   // sets parameters for declare vareable
    UStaticMeshComponent* MeshComponent;            // declaration a variable with StaticMesh class
    
    UPROPERTY()
    ASnakeBase* SnakeOwner;     // declaration a variable with SnakeBase class for definition the Snake's owner

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    
    UFUNCTION(BlueprintNativeEvent)                 // sets parameter for functions
    void SetFirstElementType();                     // declaration the function
    void SetFirstElementType_Implementation();
    
    virtual void Interact(AActor* Interactor, bool bIsHead) override;   // virtual re-declaration function Interact
    
    UFUNCTION()
    void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                            AActor* OtherActor,
                            UPrimitiveComponent* OtherComp,
                            int32 OtherBodyIndex,
                            bool bFrommSweep,
                            const FHitResult &SweepResult);
    
    UFUNCTION()
    void ToggleCollision();
};
