// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));    // impementation variable with value sub object StaticMeshComponent and title "MeshComponent"
    MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);   // sets collision paramenter, collision enabled for query only
    MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);      // sets response parameters

}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASnakeElementBase::SetFirstElementType_Implementation()    // sets default implementation, equal method apply in BP
{
    MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);    // add in Mesh overlap
}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)  // virtual re-implementation function Interact
{
    auto Snake = Cast<ASnakeBase>(Interactor);  // implementation variable Interactor with SnakeBase class
    if(IsValid(Snake))
    {
        Snake->Destroy();   // destroy the Snake
        
    }
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                           AActor* OtherActor,
                                           UPrimitiveComponent* OtherComp,
                                           int32 OtherBodyIndex,
                                           bool bFrommSweep,
                                           const FHitResult &SweepResult)   //
{
    if(IsValid(SnakeOwner))
    {
        SnakeOwner->SnakeElementOverlap(this, OtherActor);
    }
}


void ASnakeElementBase::ToggleCollision()   // function just for toggle collision
{
    if(MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
    {
        MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    }
    else
    {
        MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    }
}
